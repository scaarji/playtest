import { render } from 'react-dom';
import React from 'react';

import { Router, IndexRoute, Route, browserHistory } from 'react-router';

import App from './components/App.jsx';
import Game from './components/Game.jsx';
import Editor from './components/Editor.jsx';
import Menu from './components/Menu.jsx';

import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();

render((
  <Router history={browserHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={Menu} />
      <Route path="/game" component={Game} />
      <Route path="/editor" component={Editor} />
    </Route>
  </Router>
), document.getElementById('app'));
