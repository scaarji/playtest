import React from 'react';
import Player from './Player.jsx';

const CELL_WIDTH = 100;
const CELL_HEIGHT = 100;

export default React.createClass({
  handleClick(e) {
    if (!this.props.isAvailable) {
      return;
    }
    this.props.move(this.props.cell);
  },

  render() {
    const cell = this.props.cell;
    const players = this.props.players.map(player => {
      return (
        <Player
          key={player.id}
          player={player}
          isCurrent={this.props.currentPlayer === player}
        />
      );
    });
    const style = {
      width: CELL_WIDTH,
      height: CELL_HEIGHT,
      top: CELL_HEIGHT * cell.props.y,
      left: CELL_WIDTH * cell.props.x,
    };
    let className = 'cell';
    if (this.props.isAvailable) {
      className += ' available';
    }

    return (
      <div className={className} style={style} onClick={this.handleClick}>
        <div className="cell-label">{cell.name}</div>
        <div className="cell-players">{players}</div>
      </div>
    );
  }
});
