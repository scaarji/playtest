import React from 'react';
import { Link } from 'react-router';

export default React.createClass({
  render() {
    return (
      <ul className="main-menu">
        <li className="main-menu-entry">
          <Link className="main-menu-entry-link" to="/game">Play</Link>
        </li>
        <li className="main-menu-entry">
          <Link className="main-menu-entry-link" to="/editor">Create</Link>
        </li>
      </ul>
    );
  }
});
