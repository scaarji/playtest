import React from 'react';

export default React.createClass({
  render() {
    let className = 'player';
    if (this.props.isCurrent) {
      className += ' current';
    }
    return (<div className={className}>{this.props.player.name[0]}</div>);
  }
});
