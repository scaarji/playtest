import _ from 'lodash';
import React from 'react';

import FieldEditorModel from '../models/editor/field';
import PlayersEditorModel from '../models/editor/players';

import Field from './Editor/Field.jsx';
import Players from './Editor/Players.jsx';
import Export from './Editor/Export.jsx';

const STEP_FIELD = 'field';
const STEP_PLAYERS = 'players';
const STEP_EXPORT = 'export';
const STEPS = [
  STEP_FIELD,
  STEP_PLAYERS,
  STEP_EXPORT
];

export default React.createClass({
  getInitialState() {
    return {
      step: STEP_FIELD,
      field: new FieldEditorModel(15, 10),
      players: new PlayersEditorModel()
    }
  },

  nextStep() {
    if (this.state.step === STEP_EXPORT) {
      return;
    }

    const currentIndex = STEPS.indexOf(this.state.step);
    const nextIndex = currentIndex + 1;
    const nextStep = STEPS[nextIndex];

    this.setState({ step: nextStep });
  },

  render() {
    const currentScreen = (() => {
      const field = this.state.field;
      const players = this.state.players;
      switch (this.state.step) {
        case STEP_FIELD:
          return (<Field field={field} onFinish={this.nextStep} />);
        case STEP_PLAYERS:
          return (<Players players={players} field={field} onFinish={this.nextStep} />);
        case STEP_EXPORT:
          return (<Export field={field} players={players} />);
        default:
          return 'editor failed, try restarting';
      }
    })();

    return (
      <div className="editor">{currentScreen}</div>
    );
  }
});
