import React from 'react';

import TopMenu from './TopMenu.jsx';

export default React.createClass({
  render() {
    return (
      <div>
        <TopMenu />
        {this.props.children}
      </div>
    );
  }
});
