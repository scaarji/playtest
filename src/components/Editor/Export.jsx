import _ from 'lodash';
import React from 'react';

export default React.createClass({
  render() {
    const exportObject = _.extend({}, this.props.field.export(), this.props.players.export());
    const json = JSON.stringify(exportObject, null, 2);
    return (
      <div className="editor-screen">
        <h2 className="editor-screen-title">Export</h2>
        <textarea className="editor-export-text" defaultValue={json} readonly />
      </div>
    );
  }
});
