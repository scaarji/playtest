import React from 'react';

import Player from './Player.jsx';
import AddPlayer from './AddPlayer.jsx';

export default React.createClass({
  handleFinish(e) {
    e.preventDefault();
    this.props.onFinish();
  },

  addPlayer(player) {
    this.props.players.addPlayer(player);
    this.forceUpdate();
  },

  render() {
    const players = this.props.players;
    const list = players.playersCnt ? players.getPlayers().map((player) => {
      return (
        <li key={player.id}>
          <Player player={player} />
        </li>
      );
    }) : <span>Currently no players</span>;

    return (
      <div className="editor-screen">
        <h3 className="editor-screen-title">Edit Players</h3>
        <ul className="editor-players-list">
          {list}
        </ul>
        <AddPlayer field={this.props.field} onAdd={this.addPlayer} />
        <button className="editor-screen-next" onClick={this.handleFinish}>Finished</button>
      </div>
    );
  }
});
