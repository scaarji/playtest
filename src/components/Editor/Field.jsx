import _ from 'lodash';
import React from 'react';

import Cell from './Cell.jsx';

export default React.createClass({
  handleFinish(e) {
    e.preventDefault();
    this.props.onFinish();
  },

  render() {
    const field = this.props.field;
    const table = _.times(field.getRows(), (i) => {
      const row = _.times(field.getColumns(), (j) => {
        return (
          <td key={j}>
            <Cell key={i + 'x' + j} cell={field.getCell(i, j)} />
          </td>
        );
      });
      return (<tr key={i}>{row}</tr>);
    });
    return (
      <div className="editor-screen">
        <h3 className="editor-screen-title">Edit Field</h3>
        <table className="editor-field">
          <tbody>
            {table}
          </tbody>
        </table>
        <button className="editor-screen-next" onClick={this.handleFinish}>Finished</button>
      </div>
    );
  }
});
