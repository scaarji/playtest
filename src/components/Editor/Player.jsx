import React from 'react';

export default React.createClass({
  render() {
    const player = this.props.player;
    return (
      <div>{player.type}</div>
    );
  }
});
