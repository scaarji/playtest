import React from 'react';

export default React.createClass({
  handleClick(e) {
    e.preventDefault();

    this.props.cell.toggleType();
    this.forceUpdate();
  },

  render() {
    let className = 'editor-field-cell';
    if (this.props.cell.isEmpty()) {
      className += ' empty';
    }

    return (
      <button className={className} onClick={this.handleClick}>
        {this.props.cell.type}
      </button>
    );
  }
});
