import React from 'react';

export default React.createClass({
  handleClick(e) {
    e.preventDefault();

    const cell = this.props.cell;
    if (cell.isEmpty()) {
      return;
    }

    this.props.onSelect(this.props.cell);
  },

  render() {
    let className = 'editor-preview-field-cell';
    if (this.props.cell.isEmpty()) {
      className += ' empty';
    }
    if (this.props.selected) {
      className += ' selected';
    }

    return (
      <button className={className} onClick={this.handleClick}>
        {this.props.cell.type}
      </button>
    );
  }
});
