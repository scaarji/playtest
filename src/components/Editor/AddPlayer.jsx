import React from 'react';

import FieldPreview from './FieldPreview.jsx';

import Player from '../../models/editor/player';

export default React.createClass({
  getInitialState() {
    return { player: new Player() };
  },

  handleSelectStartCell(startCellId) {
    const player = this.state.player;
    player.startCellId = startCellId;
    this.setState({ player: player });
  },

  populatePlayerProperties() {
    const player = this.state.player;
    player.type = this.refs.type.value;
    player.movesCnt = this.refs.movesCnt.value;
  },

  reset() {
    this.setState(this.getInitialState());
    this.refs.type.value = Player.DEFAULT_TYPE;
    this.refs.movesCnt.value = Player.DEFAULT_MOVES_CNT;
  },

  handleFinished(e) {
    e.preventDefault();

    this.populatePlayerProperties();
    const player = this.state.player;

    if (!player.isValid()) {
      return;
    }

    this.props.onAdd(player);

    this.reset();
  },

  render() {
    const player = this.state.player;
    const typeOptions = Player.TYPES.map(type => {
      return (<option key={type} value={type}>{type}</option>);
    });
    return (
      <div className="editor-add-player">
        <h3 className="editor-add-player-title">Add player</h3>
        <div>
          <div className="editor-add-player-label">Select player type</div>
          <select className="editor-add-player-input" ref="type" defaultValue={Player.DEFAULT_TYPE}>{typeOptions}</select>
        </div>
        <div>
          <div className="editor-add-player-label">Enter number of moves</div>
          <input className="editor-add-player-input" ref="movesCnt" defaultValue={Player.DEFAULT_MOVES_CNT} type="text" />
        </div>
        <div>
          <div className="editor-add-player-label">Select start position</div>
          <FieldPreview
            onSelect={this.handleSelectStartCell}
            field={this.props.field}
            selected={player.startCellId} />
        </div>
        <div>
          <button className="editor-add-player-btn" onClick={this.handleFinished}>Add</button>
        </div>
      </div>
    );
  }
});
