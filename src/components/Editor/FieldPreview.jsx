import React from 'react';
import _ from 'lodash';

import CellPreview from './CellPreview.jsx';

export default React.createClass({
  handleSelect(cell) {
    this.props.onSelect(cell.id);
  },

  render() {
    const selected = this.props.selected;
    const field = this.props.field;
    const table = _.times(field.getRows(), (i) => {
      const row = _.times(field.getColumns(), (j) => {
        const cell = field.getCell(i, j);
        return (
          <td key={j}>
            <CellPreview
              cell={cell}
              selected={selected === cell.id}
              onSelect={this.handleSelect}
            />
          </td>
        );
      });
      return (<tr key={i}>{row}</tr>);
    });
    return (
      <div>
        <table className="editor-preview-field">
          <tbody>
            {table}
          </tbody>
        </table>
      </div>
    );
  }
});
