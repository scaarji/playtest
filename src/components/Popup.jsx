import React from 'react';
export default React.createClass({
  render() {
    return (
      <div className="popup">{this.props.children}</div>
    );
  }
});
