import React from 'react';

import Cell from './Cell.jsx';

export default React.createClass({
  render() {
    const availableCells = this.props.availableCells;
    const currentPlayer = this.props.currentPlayer;
    const positions = this.props.positions;

    const cells = this.props.cells.map(cell => {
      return (
        <Cell
          key={cell.id}
          cell={cell}
          players={positions.getByCell(cell)}
          move={this.props.handleMove}
          isAvailable={availableCells.has(cell)}
          currentPlayer={currentPlayer}
        />
      );
    });

    return (
      <div className="field">{cells}</div>
    );
  }
});
