import React from 'react';
export default React.createClass({
  onClick(e) {
    e.preventDefault();
    this.props.onClick();
  },

  render() {
    return (
      <button className="restart" onClick={this.onClick}>Restart the game</button>
    );
  }
});
