import React from 'react';
export default React.createClass({
  render() {
    return (
      <div className="winner">
        Winner:
        <div className="winner-name">{this.props.player.name}</div>
      </div>
    );
  }
});
