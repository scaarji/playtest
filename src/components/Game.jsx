import React from 'react';
import _ from 'lodash';

import playtest from '../models/playtest';

import Field from './Field.jsx';
import Winner from './Winner.jsx';
import Restart from './Restart.jsx';
import Popup from './Popup.jsx';

export default React.createClass({
  getInitialState() {
    return {
      game: playtest()
    }
  },

  move(cell) {
    this.state.game.moveTo(cell);
    this.forceUpdate();
  },

  restart() {
    this.state.game.restart();
    this.forceUpdate();
  },

  render() {
    const game = this.state.game;
    const availableCells = new Set(game.getAvailableMoves().map(move => move.target));

    const popup = game.winner ? (
      <Popup>
        <Winner player={game.winner} />
        <Restart onClick={this.restart} />
      </Popup>
    ) : '';

    return (
      <div className="game">
        <Field
          cells={game.getCells()}
          availableCells={availableCells}
          currentPlayer={game.getCurrentPlayer()}
          positions={game.getPositions()}
          handleMove={this.move} />
        {popup}
      </div>
    );
  }
});
