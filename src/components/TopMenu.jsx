import React from 'react';
import { Link } from 'react-router';

export default React.createClass({
  render() {
    return (
      <div className="top-menu">
        <Link className="top-menu-link top-menu-link-game" to="/game">Play</Link>
        <Link className="top-menu-logo" to="/">Playtest</Link>
        <Link className="top-menu-link top-menu-link-editor" to="/Editor">Editor</Link>
      </div>
    );
  }
});
