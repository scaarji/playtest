export default function createGetId() {
  let id = 0;
  return function getId() {
    id += 1;
    return id;
  };
}
