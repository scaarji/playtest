import createGetId from '../../helpers/get_id';

const TYPE_NONE = '';
const TYPE_FIELD = 'field';
const TYPE_TELEPORT = 'teleport';
const TYPE_START = 'start';
const TYPE_END = 'end';

const TYPES = [
  TYPE_FIELD,
  TYPE_TELEPORT,
  TYPE_START,
  TYPE_END,
  TYPE_NONE
];

const DEFAULT_TYPE = TYPE_NONE;

const getId = createGetId();

export default class EditorCell {
  constructor(x, y) {
    this.type = DEFAULT_TYPE;
    this.x = x;
    this.y = y;
    this.id = getId();
  }

  isEmpty() {
    return this.type === TYPE_NONE;
  }

  toggleType() {
    const currentIndex = TYPES.indexOf(this.type);
    const nextIndex = (currentIndex + 1) % TYPES.length;
    const nextType = TYPES[nextIndex];

    this.type = nextType;
  }

  export() {
    return {
      id: this.id,
      type: this.type,
      name: this.type,
      props: {
        x: this.x,
        y: this.y
      }
    }
  }
}
