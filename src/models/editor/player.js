import createGetId from '../../helpers/get_id';

const getId = createGetId()

const TYPE_THIEF = 'thief';
const TYPE_GUARD = 'guard';

const TYPES = [
  TYPE_THIEF,
  TYPE_GUARD
];

const DEFAULT_TYPE = TYPE_THIEF;
const DEFAULT_MOVES_CNT = 5;

export default class EditorPlayer {
  static get TYPES() { return TYPES; }
  static get DEFAULT_TYPE() { return DEFAULT_TYPE; }
  static get DEFAULT_MOVES_CNT() { return DEFAULT_MOVES_CNT; }

  constructor(type, movesCnt, startCellId) {
    this.id = getId();
    this.type = type;
    this.movesCnt = movesCnt;
    this.startCellId = startCellId;
  }

  isValid() {
    return this.type && this.movesCnt && this.startCellId;
  }

  export() {
    return {
      id: this.id,
      type: this.type,
      movesCnt: parseInt(this.movesCnt, 10),
      startCell: this.startCellId
    };
  }
}
