export default class EditorPlayers {
  constructor() {
    this.players = [];
  }

  getPlayers() {
    return this.players;
  }

  get playersCnt() {
    return this.players.length;
  }

  addPlayer(player) {
    this.players.push(player);
  }

  export() {
    return {
      players: this.players.map(player => player.export())
    };
  }
}
