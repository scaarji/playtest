import _ from 'lodash';

import Field from '../field';
import EditorCell from './cell';

export default class Editor {
  constructor(width = 10, height = 10) {
    this.width = width;
    this.height = height;
    this.field = _.times(height, (row) => {
      return _.times(width, (col) => new EditorCell(col, row));
    });
  }

  getRows() {
    return this.height;
  }

  getColumns() {
    return this.width;
  }

  getCell(i, j) {
    if (i < 0 || j < 0 || i >= this.getRows() || j >= this.getColumns()) {
      return undefined;
    }
    return this.field[i][j];
  }

  _getStart() {
    for (let i = 0; i < this.getRows(); i += 1) {
      for (let j = 0; j < this.getColumns(); j += 1) {
        const cell = this.getCell(i, j);
        if (cell.type === 'start') {
          return cell;
        }
      }
    }

    return null;
  }

  _getNeighbors(cell) {
    return _([
      this.getCell(cell.y - 1, cell.x),
      this.getCell(cell.y + 1, cell.x),
      this.getCell(cell.y, cell.x - 1),
      this.getCell(cell.y, cell.x + 1)
    ]).compact().reject(cell => cell.isEmpty()).value();
  }

  export() {
    const start = this._getStart();
    if (!start) {
      return '';
    }

    const field = new Field();
    field.addCell(start.export());

    let from = [start];
    while (from.length) {
      const to = _(from)
        .map((fromCell) => {
          const neighbors = this._getNeighbors(fromCell);
          const newCells = _.reject(neighbors, (cell) => field.hasCell(cell.id));
          neighbors.forEach((toCell) => {
            field.addCell(toCell.export());
            field.addConnection(fromCell.id, toCell.id);
          });
          return newCells;
        })
        .flatten()
        .value();

      from = to;
    }

    const cells = field.getCells();
    return {
      cells: cells,
      connections: _(cells)
        .pluck('id')
        .map(fromId => {
          return field.getConnections(fromId).map((toId) => ({ from: fromId, to: toId }));
        })
        .flatten()
        .value()
    };
  }
}
