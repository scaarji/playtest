'use strict';
export default class WinChecker {
  getWinner(game) {
    const winners = game.getPlayers().filter(player => {
      return (player.type === 'thief' && game.getPositions().get(player).type === 'end');
    });

    return winners && winners[0];
  }
}
