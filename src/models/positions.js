'use strict';
export default class Positions {
  constructor() {
    this.positions = new Map();
    this.startPositions = [];
  }

  reset() {
    this.positions = new Map(this.startPositions);
  }

  set(player, cell) {
    if (!this.positions.has(player)) {
      this.startPositions.push([player, cell]);
    }

    this.positions.set(player, cell);
  }

  get(player) {
    return this.positions.get(player);
  }

  getByCell(cell) {
    return Array.from(this.positions.keys())
      .filter(player => this.positions.get(player) === cell);
  }
}
