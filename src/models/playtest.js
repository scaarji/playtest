'use strict';
import Game from './game';
import Player from './player';
import Field from './field';
import WinChecker from './win_checker';

import GameBuilder from './game_builder';

import level from '../levels/2.json';

export default function() {
  const gameBuilder = new GameBuilder();
  gameBuilder.createField(level.cells, level.connections);

  level.players.forEach(playerInfo => gameBuilder.addPlayer(playerInfo));

  const game = gameBuilder.build();

  return game.start();
}
