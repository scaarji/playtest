'use strict';
import _ from 'lodash';
export default class Field {
  constructor() {
    this.cells = new Map();
    this.connections = new Map();
  }

  getCells() {
    return Array.from(this.cells.values());
  }

  getCell(id) {
    return this.cells.get(id);
  }

  hasCell(id) {
    return this.cells.has(id);
  }

  addCell(cell) {
    if (!this.cells.has(cell.id)) {
      this.cells.set(cell.id, cell);
      this.connections.set(cell.id, new Set());
    }

    return this;
  }

  getConnections(fromId) {
    return Array.from(this.connections.get(fromId));
  }

  addConnection(fromId, toId) {
    this.connections.get(fromId).add(toId);
    this.connections.get(toId).add(fromId);

    return this;
  }

  filterCells(fn) {
    return this.getCells().filter(fn);
  }

  getCellsInRange(start, range) {
    const _findCells = (acc, from, currentRange, visitedCells) => {
      if (!from.length || currentRange > range) {
        return acc;
      }

      const to = _(from)
        .pluck('id')
        .map(this.getConnections.bind(this))
        .flatten()
        .map(this.getCell.bind(this))
        .reject(visitedCells.has.bind(visitedCells))
        .value();

      const updatedVisitedCells = to.reduce((cells, cell) => cells.add(cell), visitedCells);
      const diffAcc = to.map(cell => { return { cell, distance: currentRange }; });

      return _findCells(acc.concat(diffAcc), to, currentRange + 1, updatedVisitedCells);
    }

    const resultSet = _findCells([], [start], 1, new Set([start]));
    return Array.from(resultSet);
  }
}
