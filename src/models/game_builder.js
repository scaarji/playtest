import WinChecker from './win_checker';
import Field from './field';
import Player from './player';
import Game from './game';
import createCell from './cell';

export default class GameBuilder {
  constructor() {
    this.field = new Field();
    this.players = [];
    this.startPositions = [];
    this.winChecker = new WinChecker();
  }

  createField(cells, connections) {
    cells.forEach(cell => {
      this.field.addCell(createCell(cell.type, cell.id, cell.name, cell.props));
    });
    connections.forEach(connection => {
      this.field.addConnection(connection.from, connection.to);
    });
    return this;
  }

  addPlayer(playerInfo) {
    const player = new Player(
      playerInfo.type,
      playerInfo.id,
      playerInfo.name,
      playerInfo.movesCnt
    );
    this.players.push(player);
    this.startPositions.push({
      player,
      cell: this.field.getCell(playerInfo.startCellId)
    });
  }

  build() {
    const game = new Game(this.field, this.players, this.winChecker);
    this.startPositions.forEach(({ player, cell }) => game.setStartPosition(player, cell));
    return game;
  }
}
