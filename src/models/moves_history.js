import _ from 'lodash';
export default class MovesHistory {
  constructor() {
    this.history = [];
  }

  push(move) {
    this.history.push(move);
  }

  getLatestByPlayer(player) {
    return _.takeRightWhile(this.history, move => move.player === player);
  }
}
