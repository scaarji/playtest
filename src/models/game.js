'use strict';
import _ from 'lodash';

import Positions from './positions';
import MovesHistory from './moves_history';

export default class Game {
  constructor(field, players, winChecker) {
    this.field = field;
    this.players = players;
    this.positions = new Positions();
    this.winChecker = winChecker;
    this.winner = null;
    this.turn = null;
    this.history = new MovesHistory();
  }

  start() {
    this.createTurn(this.getPlayer());
    return this;
  }

  restart() {
    this.turn = null;
    this.winner = null;
    this.history = new MovesHistory();
    this.positions.reset();

    this.start();
  }

  isFirstTurn() {
    return !this.turn;
  }

  getPlayer() {
    if (this.isFirstTurn()) {
      return this.getFirstPlayer();
    }

    return this.getNextPlayer();
  }

  getCurrentPlayer() {
    return this.turn.player;
  }

  getFirstPlayer() {
    return _.first(this.players);
  }

  getNextPlayer() {
    const currentPlayer = this.turn.player;
    const index = this.players.indexOf(currentPlayer);
    const nextIndex = (index + 1) % this.players.length;
    const nextPlayer = this.players[nextIndex];

    return nextPlayer;
  }

  createTurn(player) {
    const currentCell = this.positions.get(player);
    const playerHistory = this.history.getLatestByPlayer(player);
    const availableMoves = currentCell.getAvailableMoves(this.field, player, playerHistory);

    if (!availableMoves.length) {
      return this.createTurn(this.getNextPlayer());
    }

    this.turn = { player, availableMoves };
  }

  moveTo(target) {
    if (this.winner) {
      return this;
    }
    const move = _.find(this.turn.availableMoves, { target });
    if (!move) {
      return this;
    }
    this.history.push(move);

    const currentPlayer = this.getCurrentPlayer();
    this.positions.set(currentPlayer, target);

    const winner = this.winChecker.getWinner(this);
    if (winner) {
      this.winner = winner;
      return this;
    }

    this.createTurn(currentPlayer);

    return this;
  }

  setStartPosition(player, cell) {
    this.positions.set(player, cell);
    return this;
  }

  getAvailableMoves() {
    return this.turn.availableMoves;
  }

  getPositions() {
    return this.positions;
  }

  getCells() {
    return this.field.getCells();
  }

  getCellById(id) {
    return this.field.getCell(id);
  }

  getPlayers() {
    return this.players;
  }

  getPlayerById(id) {
    return _.findWhere(this.players, { id });
  }
}
