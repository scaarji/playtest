'use strict';
import FieldCell from './field';
import TeleportCell from './teleport';
import StartCell from './start';
import EndCell from './end';

export default function createCell(type, id, name, props) {
    switch (type) {
        case 'field': return new FieldCell(type, id, name, props);
        case 'start': return new StartCell(type, id, name, props);
        case 'end': return new EndCell(type, id, name, props);
        case 'teleport': return new TeleportCell(type, id, name, props);
        default: throw new Error('Unknown cell type');
    }
}
