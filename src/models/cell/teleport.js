'use strict';
import _ from 'lodash';
import BaseCell from './base';
export default class TeleportCell extends BaseCell {
  getAvailableMoves(field, player, history) {
    const lastMove = _.last(history);
    if (lastMove && lastMove.type === 'default') {
      const teleports = field.filterCells(cell => {
        return cell !== this && cell instanceof TeleportCell;
      });
      const index = _.random(0, teleports.length - 1);
      const nextTeleport = teleports[index];
      return [
        {
          target: nextTeleport,
          player: player,
          type: 'teleport',
          cost: 0
        }
      ];
    }

    return super.getAvailableMoves(field, player, history);
  }
}
