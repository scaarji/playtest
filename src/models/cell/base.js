'use strict';
import _ from 'lodash';
export default class BaseCell {
  constructor(type, id, name, props) {
    this.type = type;
    this.id = id;
    this.name = name;
    this.props = props;
  }

  getAvailableMoves(field, player, history) {
    const movesLeft = player.getMovesCnt() - _(history).pluck('cost').sum();
    if (!movesLeft) {
      return [];
    }

    const availableCells = field.getCellsInRange(this, movesLeft);
    const availableMoves = availableCells.map(({ cell, distance }) => {
      return {
        target: cell,
        player: player,
        type: 'default',
        cost: distance
      };
    });

    return availableMoves;
  }
}
