'use strict';
export default class Player {
  constructor(type, id, name, movesCnt) {
    this.type = type;
    this.id = id;
    this.name = name;
    this.movesCnt = movesCnt;
  }

  getMovesCnt() {
    return this.movesCnt;
  }
}
